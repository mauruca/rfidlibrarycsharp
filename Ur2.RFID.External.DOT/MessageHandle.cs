﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace Ur2.RFID.External.DOT
{
    public struct Message {
        public int Msg;
        public IntPtr WParam;
        public IntPtr LParam;
    }

    public class MessageWindowHandle : SafeHandle
    {
        //Mohammad Elsheimy - https://opensource.org/licenses/cpl1.0.php
        [DllImport("Kernel32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool CloseHandle(IntPtr hObject);

        // If ownsHandle equals true handle will
        // be automatically released during the
        // finalization process, otherwise, you
        // will have the responsibility to
        // release it outside the class.
        // Automatic releasing means calling
        // the ReleaseHandle() method.
        public MessageWindowHandle() : base(IntPtr.Zero, true)
        {

        }

        public MessageWindowHandle
            (IntPtr preexistingHandle, bool ownsHandle = true)
            : base(IntPtr.Zero, ownsHandle)
        {
            this.SetHandle(preexistingHandle);
        }

        protected virtual void WndProc(ref Message m)
        {
            // nao faço nada com a mesnagem ainda
        }

        public IntPtr Hwnd
        {
            get { return this.handle; }
        }

        public override bool IsInvalid
        {
            get
            {
                return this.handle == IntPtr.Zero;
            }
        }

        protected override bool ReleaseHandle()
        {
            return CloseHandle(this.handle);
        }
        /* Fim Mohammad Elsheimy*/
    }
}
