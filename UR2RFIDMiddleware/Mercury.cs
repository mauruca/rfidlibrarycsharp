﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ur2.RFID.Middleware;
using Ur2.RFID.Middleware.Modelo;
using ThingMagic;
using System.Threading;

namespace Ur2.RFID.Middleware
{
    public class Mercury : DeviceBase<Mercury>, IDevice
    {
        private string _url = string.Empty;
        Reader _activeReader = null;
        private bool _reading = false;
        private object lockRead = new object();
        private object lockActive = new object();

        public string URL
        {
            get { return _url; }
            set { _url = value.Trim(); }
        }

        private Reader ActiveReader
        {
            get{ return _activeReader; }
            set { _activeReader = value; }
        }

        public Mercury()
        {
            _deviceType = DeviceType.ThingMagicM6;
        }

        #region Interface methods

        private void ReadExceptionHandler(Object sender, ReaderExceptionEventArgs e)
        {
            //AddMessage(e.ReaderException.Message);
            Console.WriteLine(e.ReaderException.ToString());
        }

        public void DefineWindowHaldle()
        {
            if (URL.Length == 0)
            {
                Memory.AddMessage("Address is missing.Ex: tmr://192.168.0.2");
                return;
            }

            try
            {
                ActiveReader = Reader.Create(URL);
                ActiveReader.Connect();
            }
            catch (Exception e)
            {
                Memory.AddMessage(e.Message);
            }
        }

        public bool Open(string url = "")
        {
            if (url.Trim().Length == 0)
                return false;

            URL = TrataURL(url);

            if (!Monitor.TryEnter(this))
            {
                return IsActive;
            }
            try
            {
                if (!_isActive)
                {
                    DefineWindowHaldle();
                    ConfigReader();
                    _isActive = true;
                }
            }
            catch (ReaderException e)
            {
                _isActive = false;
                Memory.AddMessage(e.Message);
            }
            finally
            {
                Monitor.PulseAll(this);
                Monitor.Exit(this);
            }
            return IsActive;
        }

        public void Close()
        {
            if (!Monitor.TryEnter(this))
                return;
            try
            {
                StopReading();
                if (_isActive)
                {
                    ActiveReader.Destroy();
                    _isActive = false;
                }
            }
            finally
            {
                Monitor.PulseAll(this);
                Monitor.Exit(this);
            }
        }

        public void StartReading(bool continuous = false, bool clearDeviceMemory = true)
        {
            if (!Monitor.TryEnter(this))
            {
                return;
            }

            try
            {
                if (clearDeviceMemory)
                    Memory.Clear();

                ActiveReader.TagRead += new EventHandler<TagReadDataEventArgs>(ProcessRead);
                _reading = true;
                if (continuous)
                {
                    ActiveReader.StartReading();
                    return;
                }
                ReadOnceSync();
            }
            catch (ReaderException re)
            {
                Memory.AddMessage(re.Message);
            }
            catch (Exception e)
            {
                Memory.AddMessage(e.Message);
            }
            finally
            {
                Monitor.Exit(this);
            }
        }
        
        public void StopReading()
        {
            if (ActiveReader == null)
                return;
            if (!Monitor.TryEnter(this))
            {
                return;
            }
            try
            {
                if (_reading)
                {
                    ActiveReader.StopReading();
                    ActiveReader.TagRead -= ProcessRead;
                    _reading = false;
                }
            }
            catch (Exception e)
            {
                Memory.AddMessage(e.Message);
            }
            finally
            {
                Monitor.Exit(this);
            }

        }

        #endregion Interface methods

        #region Methods
        private void ReadOnceSync(int miliseconds = 100)
        {
            ReadTagArray(ActiveReader.Read(miliseconds));
        }

        private void ReadOnceAsync(int timeoutMilliseconds = 100)
        {
            ActiveReader.StartReading();
            Thread.Sleep(timeoutMilliseconds);
            StopReading();
        }

        private void SetSupportedRegion()
        {
            if (Reader.Region.UNSPEC == (Reader.Region)ActiveReader.ParamGet("/reader/region/id"))
            {
                Reader.Region[] supportedRegions = (Reader.Region[])ActiveReader.ParamGet("/reader/region/supportedRegions");
                if (supportedRegions.Length < 1)
                {
                    throw new FAULT_INVALID_REGION_Exception();
                }
                else
                {
                    ActiveReader.ParamSet("/reader/region/id", supportedRegions[0]);
                }
            }
        }

        private void ConfigReader()
        {
            SetSupportedRegion();

            ActiveReader.ParamSet("/reader/read/asyncOnTime", 100);
            ActiveReader.ParamSet("/reader/read/asyncOffTime", 0);

            ActiveReader.ReadException += ReadExceptionHandler;
        }

        private void ProcessRead(Object sender, TagReadDataEventArgs e)
        {
            ReadOneTag(e.TagReadData);
        }

        private void ReadOneTag(TagReadData tag)
        {
            if (tag == null)
                return;
            Memory.AddTag(tag.EpcString);
            OnReadTag();
        }

        private void ReadTagArray(TagReadData[] tags)
        {
            if (tags == null || tags.Length == 0)
                return;

            foreach (TagReadData item in tags)
            {
                Memory.AddTag(item.EpcString);
            }
            OnReadTag();
        }

        private string TrataURL(string url)
        {
            if (url.Trim().Length == 0)
                return string.Empty;
            url = url.Trim();
            if (url.Contains("tmr://"))
                return url;
            return "tmr://" + url;
        }

        #endregion Methods
    }
}
