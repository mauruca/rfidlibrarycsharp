﻿namespace Ur2.RFID.Middleware.Modelo
{
    public abstract class SingletonBaseClass<T> where T : class, new()
    {
        private volatile static T _instance;

        private static object _lockObj = new object();

        public static T Instance()
        {
            if (_instance == null)
            {
                lock (_lockObj)
                {
                    if (_instance == null)
                        _instance = new T();
                }
            }
            return _instance;
        }
    }
}