﻿using System;
using System.Collections.Generic;

namespace Ur2.RFID.Middleware.Modelo
{
    public class DeviceMemory
    {
        #region Fields
        private RFIDTag _lastTag = new RFIDTag();
        private List<RFIDTag> _tags = new List<RFIDTag>();
        private List<string> deviceMessages = new List<string>();
        #endregion Fields

        #region Properties
        public bool isLastTagValid
        {
            get
            {
                return (LastTag != null && LastTag.isValid);
            }
        }
        public RFIDTag LastTag
        {
            get { return _lastTag; }
        }
        public List<RFIDTag> Tags
        {
            get { return _tags; }
        }
        public int TagsReadQty
        {
            get { return Tags.Count; }
        }
        #endregion Properties

        public void Clear()
        {
            ClearLastTag();
            ClearTagList();
            ClearMessages();
        }
        public void ClearLastTag()
        {
            _lastTag = null;
        }
        public void ClearTagList()
        {
            _tags.Clear();
        }
        public void AddTag(string tagread)
        {
            if (tagread == null || tagread.Trim().Length == 0)
                return;

            _lastTag = Tags.Find(item => item.Id == tagread);
            // Se nao encontrou cria um novo e adiciona a lista de tags
            if (_lastTag == null)
            {
                _lastTag = new RFIDTag(tagread);
                Tags.Add(LastTag);
            }
            LastTag.MaisUmRegistro();
        }

        #region Messages
        public void AddMessage(string message)
        {
            if (message.Trim().Length == 0)
                deviceMessages.Add("Mensagem vazia.");
            deviceMessages.Add(message.Trim());
            Console.WriteLine(message);
        }
        public List<string> GetMessages()
        {
            return deviceMessages;
        }
        public void ClearMessages()
        {
            deviceMessages.Clear();
        }
        #endregion Messages
    }
}