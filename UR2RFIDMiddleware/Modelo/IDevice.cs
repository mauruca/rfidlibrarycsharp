﻿using System;
using System.Collections.Generic;
using Ur2.RFID.Middleware.Modelo;

namespace Ur2.RFID.Middleware.Modelo
{
    public enum DeviceType
    {
        ThingMagicM6,
        DOTR900
    }

    public interface IDevice
    {
        DeviceType Type { get; }

        DeviceMemory Memory { get; }

        bool IsActive { get; }

        UInt32 BuzzVolume { set; }

        void DefineWindowHaldle();

        bool Open(string address = "");

        void Close();
        
        void StartReading(bool continuous = false, bool clearDeviceMemory = true);

        void StopReading();

        #region ClockMethods
        void StartTickerChange();
        void StopTickerChange();
        #endregion ClockMethods

        void ConfigTickerChangeElapseTime(int seconds = 1);

        event ChangeEventHandler OnRead;

        event ChangeEventHandler OnTimerTick;
    }
}