﻿namespace Ur2.RFID.Middleware.Modelo
{
    public class RFIDTag
    {
        private string _id = string.Empty;
        private int _qtd = 0;

        public string Id
        {
            get { return _id; }
            set {
                if (value == null)
                {
                    _id = string.Empty;
                    return;
                }
                _id = value.Trim();
            }
        }
        public int Qtd
        {
            get { return _qtd; }
        }
        public bool isValid
        {
            get
            {
                if (_id == null || _id.Length == 0)
                    return false;
                return true;
            }
        }

        public RFIDTag()
        { }
        public RFIDTag(string id)
        {
            Id = id;
        }

        public void MaisUmRegistro()
        {
            _qtd++;
        }
        public override string ToString()
        {
            return Id;
        }
        public override bool Equals(object obj)
        {
            return ( obj is RFIDTag && (Id.Equals(((RFIDTag)obj).Id)));
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}