﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ur2.RFID.Middleware.Modelo;
using System.Timers;

namespace Ur2.RFID.Middleware
{
    public delegate void ChangeEventHandler();

    public abstract class DeviceBase<T> where T : class, new()
    {
        #region Singleton
        private volatile static T _instance;

        private static object _lockObj = new object();

        public static T Instance()
        {
            if (_instance == null)
            {
                lock (_lockObj)
                {
                    if (_instance == null)
                    {
                        _instance = new T();
                    }
                }
            }
            return _instance;
        }
        #endregion Singleton

        #region Attributes
        internal DeviceType _deviceType;
        internal bool _isActive = false;
        private DeviceMemory _memory = new DeviceMemory();
        private System.Timers.Timer tickerChange = new System.Timers.Timer();
        #endregion Attributes

        #region Properties
        public DeviceType Type
        {
            get { return _deviceType; }
        }
        public DeviceMemory Memory
        {
            get { return _memory; }
        }

        public virtual bool IsActive
        {
            get { return _isActive; }
        }
        #endregion Properties

        public DeviceBase()
        {
            tickerChange.Elapsed += OnTick;
        }

        public virtual uint BuzzVolume
        {
            set { throw new NotImplementedException("Sou um dispositivo silencioso e não faço barulho."); }
        }

        public event ChangeEventHandler OnRead;

        protected void OnReadTag()
        {
            if (OnRead != null)
                OnRead();
        }

        #region Clock Methods
        public void ConfigTickerChangeElapseTime(int seconds = 1)
        {
            tickerChange.Interval = seconds * 1000;
        }
        public void StartTickerChange()
        {
            tickerChange.Enabled = true;
        }
        public void StopTickerChange()
        {
            tickerChange.Enabled = false;
        }

        public event ChangeEventHandler OnTimerTick;

        public void OnTick(object source, ElapsedEventArgs e)
        {
            if (OnTimerTick != null)
                OnTimerTick();
        }
        #endregion Clock Methods
    }
}
