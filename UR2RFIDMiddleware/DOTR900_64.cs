﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Threading;
using System.Timers;
using System.Runtime.InteropServices;
using Ur2.RFID.Middleware.Modelo;
using Ur2.RFID.External.DOT;


namespace Ur2.RFID.Middleware
{
    public class BuzzVolume_64
    {
        public static UInt64 Mute = 0;
        public static UInt64 Low = 1;
        public static UInt64 High = 2;
    }

    public class DOTR900_64 : DeviceBase<DOTR900_32>, IDevice
    {
        [DllImport("R900Lib.dll", EntryPoint = "R900LIB_SetBuzzerVolume", CallingConvention=CallingConvention.Cdecl)]
        unsafe internal static extern bool _R900LIB_SetBuzzerVolume(UInt64 value, bool nv);

        #region Atributos
        private UHFAPI_NET_64 _R900APP;
        #endregion Atributos

        #region Propriedades
        public UHFAPI_NET_64 R900APP
        {
            get
            {
                if (_R900APP == null)
                {
                    _R900APP = new UHFAPI_NET_64();
                }
                return _R900APP;
            }
        }
        public override uint BuzzVolume
        {
            set { _R900LIB_SetBuzzerVolume(value, false); }
        }
        #endregion Propriedades

        public DOTR900_64()
        {
            _deviceType = DeviceType.DOTR900;
            _R900APP = new UHFAPI_NET_64();
            ConfigTickerChangeElapseTime();
        }

        #region Interface methods
        public void DefineWindowHaldle()
        {
            //window handle
            R900APP.UHFAPI_HWND_EX(0, 0);
        }

        public bool Open(string address = "")
        {
            // address não usado para DOT 900
            DefineWindowHaldle();
            if (R900APP.UHFAPI_Open())
            {
                SetActive();
                AddLocalHandlers();
                return true;
            }
            return false;
        }
        public void Close()
        {
            RemoveLocalHandlers();
            R900APP.UHFAPI_Close();
            SetActive();
        }

        public void SetActive()
        {
            _isActive = R900APP.UHFAPI_IsOpen();
        }

        /// <summary>
        /// Inicia a pesquisa de TAG.
        /// </summary>
        /// <param name="continuous">True para pesquisa continua e false ou vazio para somente uma pesquisa.</param>
        public void StartReading(bool continuous = false, bool clearDeviceMemory = true)
        {
            if (clearDeviceMemory)
                Memory.Clear();

            if (!Open())
                return;
            _R900APP.evtInventoryEPC += new UHFAPI_NET_64.InventoryEPCDispacher(ProcessRead);
            // start inventory
            R900APP.UHFAPI_Inventory(InventoryParameter(continuous), false);
        }

        /// <summary>
        /// Para a pesquisa de TAG.
        /// </summary>
        public void StopReading()
        {
            R900APP.UHFAPI_Stop();
            _R900APP.evtInventoryEPC -= ProcessRead;
        }
        #endregion Interface methods

        #region Methods

        /// <summary>
        /// Monta os parametros do leitor
        /// </summary>
        /// <param name="continuous">True para pesquisa continua e false ou vazio para somente uma pesquisa.</param>
        /// <returns>Parametros do leitor</returns>
        private UHFAPI_NET_64.structTAG_OP_PARAM InventoryParameter(bool continuous = false)
        {
            UHFAPI_NET_64.structTAG_OP_PARAM parameters = new UHFAPI_NET_64.structTAG_OP_PARAM();

            parameters.single_tag = (continuous ? (int)0 : (int)1);
            parameters.QuerySelected = (int)0;
            parameters.timeout = 0; // 0 means endless
            parameters.mask.mskBits = 0; // do not use mask

            return parameters;
        }

        private void ProcessRead(string tagread)
        {
            Memory.AddTag(tagread.Substring(4));
            OnReadTag();
        }

        #endregion Methods

        #region TriggerHandlers

        #region LocalHandlers
        private void AddLocalHandlers()
        {
            // message handler
            R900APP.evtR900TriggerEvent += new UHFAPI_NET_64.R900TriggerHandler(R900TriggerHandler);
            R900APP.evtCmdBegin += new UHFAPI_NET_64.BeginEventDispacher(R900CommandBegin);
            R900APP.evtCmdEnd += new UHFAPI_NET_64.EndEventDispacher(R900CommandEnd);
            R900APP.evtLinkLost += new UHFAPI_NET_64.LinkLostEventHandler(R900APP_evtLinkLost);
            R900APP.evtPlatformPowerResume += new UHFAPI_NET_64.PlatformPowerResumeEventHandler(R900APP_evtPlatformPowerResume);
        }

        private void RemoveLocalHandlers()
        {
            // message handler
            R900APP.evtR900TriggerEvent -= R900TriggerHandler;
            R900APP.evtCmdBegin -= R900CommandBegin;
            R900APP.evtCmdEnd -= R900CommandEnd;
            R900APP.evtLinkLost -= R900APP_evtLinkLost;
            R900APP.evtPlatformPowerResume -= R900APP_evtPlatformPowerResume;
        }

        private void R900CommandBegin(string cmd)
        {
        }

        private void R900CommandEnd(string cmd)
        {
        }

        private void R900TriggerHandler(bool trigger_on)
        {
        }
        private void R900APP_evtLinkLost()
        {
            Close();
        }

        private void R900APP_evtPlatformPowerResume()
        {
            Close();
        }
        #endregion LocalHandlers

        #region ClientHandlers

        public delegate void DTR900LinkLostEventHandler();

        public delegate void DTR900PowerResumeEventHandler();

        public delegate void DTR900ReadTagEventHandler(string tag);

        public void LinkLostEvent(DTR900LinkLostEventHandler linkLostEventMethod)
        {
            R900APP.evtLinkLost += new UHFAPI_NET_64.LinkLostEventHandler(linkLostEventMethod);
        }

        public void PowerResumeEvent(DTR900PowerResumeEventHandler powerResumeEventMethod)
        {
            R900APP.evtPlatformPowerResume += new UHFAPI_NET_64.PlatformPowerResumeEventHandler(powerResumeEventMethod);
        }

        public void ReadTagEvent(DTR900ReadTagEventHandler readTagEventMethod)
        {
            R900APP.evtInventoryEPC += new UHFAPI_NET_64.InventoryEPCDispacher(readTagEventMethod);
        }
        #endregion ClientHandlers
        
        #endregion TriggerHandlers

    }
}