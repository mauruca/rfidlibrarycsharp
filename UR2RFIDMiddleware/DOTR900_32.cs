﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Threading;
using System.Timers;
using System.Runtime.InteropServices;
using Ur2.RFID.Middleware.Modelo;
using Ur2.RFID.External.DOT;

namespace Ur2.RFID.Middleware
{
    public class BuzzVolume_32
    {
        public static UInt32 Mute = 0;
        public static UInt32 Low = 1;
        public static UInt32 High = 2;
    }

    // report mode bit field definition
    public class ReportMode
    {
        public static uint REPORT_MODE_WAIT_DONE = 1; // han 2008.12.11
                                                      // default disabled; do not return API function until operation d finished. No message are generated
        public static uint REPORT_MODE_ACCESS_EPC = 2; // han 2008.12.11
                                                       // default enabled; do not generate message for the EPC report. EPC can be read when operation is finished successfully
        public static uint REPORT_MODE_EXTENDED_INFORMATION = 4; // han 2008.12.16
                                                                 // default disabled; report epc with additional auxiliary information
        public static uint REPORT_MODE_BATTERY_FAULT = 8; // han 2009.10.12
                                                          // default enabled; check battery fault
        public static uint REPORT_MODE_THREAD_MESSAGE = 16; // han 2010.1.11
                                                            // default disabled; application handle is thread id. use thread message for message 
        public static uint REPORT_MODE_NO_EPC_CRC = 32; // han 2010.1.11
                                                        // default enabled; do not report crc word for epc
        public static uint REPORT_MODE_RFTX_INFO = 64; // han 2010.5.12
                                    // default disabled; for administrator use only
    }

    public class DOTR900_32 : DeviceBase<DOTR900_32>, IDevice
    {
        [DllImport("R900Lib.dll", EntryPoint = "R900LIB_SetBuzzerVolume", CallingConvention=CallingConvention.Cdecl)]
        unsafe internal static extern bool _R900LIB_SetBuzzerVolume(UInt32 value, bool nv);

        #region Atributos
        private UHFAPI_NET_32 _R900APP;
        #endregion Atributos

        #region Propriedades
        public UHFAPI_NET_32 R900APP
        {
            get
            {
                if (_R900APP == null)
                {
                    _R900APP = new UHFAPI_NET_32();
                }
                return _R900APP;
            }
        }
        public override UInt32 BuzzVolume
        {
            set { _R900LIB_SetBuzzerVolume(value, false); }
        }
        #endregion Propriedades

        public DOTR900_32()
        {
            _deviceType = DeviceType.DOTR900;
            _R900APP = new UHFAPI_NET_32();
            ConfigTickerChangeElapseTime();
        }

        #region Interface methods
        public void DefineWindowHaldle()
        {
            //window handle
            R900APP.UHFAPI_HWND_EX(ReportMode.REPORT_MODE_ACCESS_EPC, 0);
        }

        public bool Open(string address = "")
        {
            // address não usado para DOT 900
            if (R900APP.UHFAPI_Open())
            {
                SetActive();
                AddLocalHandlers();
                return true;
            }
            return false;
        }
        public void Close()
        {
            RemoveLocalHandlers();
            R900APP.UHFAPI_Close();
            SetActive();
        }

        public void SetActive()
        {
            _isActive = R900APP.UHFAPI_IsOpen();
        }

        /// <summary>
        /// Inicia a pesquisa de TAG.
        /// </summary>
        /// <param name="continuous">True para pesquisa continua e false ou vazio para somente uma pesquisa.</param>
        public void StartReading(bool continuous = false, bool clearDeviceMemory = true)
        {
            if (clearDeviceMemory)
                Memory.Clear();

            if (!Open())
                return;
            _R900APP.evtInventoryEPC += new UHFAPI_NET_32.InventoryEPCDispacher(ProcessRead);
            // start inventory
            R900APP.UHFAPI_Inventory(InventoryParameter(continuous), false);
        }

        /// <summary>
        /// Para a pesquisa de TAG.
        /// </summary>
        public void StopReading()
        {
            R900APP.UHFAPI_Stop();
            _R900APP.evtInventoryEPC -= ProcessRead;
        }
        #endregion Interface methods

        #region Methods

        /// <summary>
        /// Monta os parametros do leitor
        /// </summary>
        /// <param name="continuous">True para pesquisa continua e false ou vazio para somente uma pesquisa.</param>
        /// <returns>Parametros do leitor</returns>
        private UHFAPI_NET_32.structTAG_OP_PARAM InventoryParameter(bool continuous = false)
        {
            UHFAPI_NET_32.structTAG_OP_PARAM parameters = new UHFAPI_NET_32.structTAG_OP_PARAM();

            parameters.single_tag = (continuous ? (int)0 : (int)1);
            parameters.QuerySelected = (int)0;
            parameters.timeout = 0; // 0 means endless
            parameters.mask.mskBits = 0; // do not use mask

            return parameters;
        }

        private void ProcessRead(string tagread)
        {
            Memory.AddTag(tagread.Substring(4));
            OnReadTag();
        }

        #endregion Methods

        #region TriggerHandlers

        #region LocalHandlers
        private void AddLocalHandlers()
        {
            // message handler
            R900APP.evtR900TriggerEvent += new UHFAPI_NET_32.R900TriggerHandler(R900TriggerHandler);
            R900APP.evtCmdBegin += new UHFAPI_NET_32.BeginEventDispacher(R900CommandBegin);
            R900APP.evtCmdEnd += new UHFAPI_NET_32.EndEventDispacher(R900CommandEnd);
            R900APP.evtLinkLost += new UHFAPI_NET_32.LinkLostEventHandler(R900APP_evtLinkLost);
            R900APP.evtPlatformPowerResume += new UHFAPI_NET_32.PlatformPowerResumeEventHandler(R900APP_evtPlatformPowerResume);
        }

        private void RemoveLocalHandlers()
        {
            // message handler
            R900APP.evtR900TriggerEvent -= R900TriggerHandler;
            R900APP.evtCmdBegin -= R900CommandBegin;
            R900APP.evtCmdEnd -= R900CommandEnd;
            R900APP.evtLinkLost -= R900APP_evtLinkLost;
            R900APP.evtPlatformPowerResume -= R900APP_evtPlatformPowerResume;
        }

        private void R900CommandBegin(string cmd)
        {
        }

        private void R900CommandEnd(string cmd)
        {
        }

        private void R900TriggerHandler(bool trigger_on)
        {
        }
        private void R900APP_evtLinkLost()
        {
            Close();
        }

        private void R900APP_evtPlatformPowerResume()
        {
            Close();
        }
        #endregion LocalHandlers

        #region ClientHandlers

        public delegate void DTR900LinkLostEventHandler();

        public delegate void DTR900PowerResumeEventHandler();

        public delegate void DTR900ReadTagEventHandler(string tag);

        public void LinkLostEvent(DTR900LinkLostEventHandler linkLostEventMethod)
        {
            R900APP.evtLinkLost += new UHFAPI_NET_32.LinkLostEventHandler(linkLostEventMethod);
        }

        public void PowerResumeEvent(DTR900PowerResumeEventHandler powerResumeEventMethod)
        {
            R900APP.evtPlatformPowerResume += new UHFAPI_NET_32.PlatformPowerResumeEventHandler(powerResumeEventMethod);
        }

        public void ReadTagEvent(DTR900ReadTagEventHandler readTagEventMethod)
        {
            R900APP.evtInventoryEPC += new UHFAPI_NET_32.InventoryEPCDispacher(readTagEventMethod);
        }
        #endregion ClientHandlers
        
        #endregion TriggerHandlers

    }
}