﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ur2.RFID.Middleware.Modelo;

namespace Ur2.RFID.Middleware
{
    public abstract class DeviceFactory
    {
        public static IDevice CreateDevice(DeviceType type)
        {
            if (DOTR900.Instance().Type == type)
                return DOTR900.Instance();
            if (Mercury.Instance().Type == type)
                return Mercury.Instance();
            return null;
        }
    }
}
