﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ur2.RFID.Middleware;

namespace Ur2.RFID.Middleware.Test
{
    [TestClass]
    public class DeviceMemoryTest
    {
        [TestMethod]
        public void TestEmptyMemoryReadQtd()
        {
            Modelo.DeviceMemory mem = new Modelo.DeviceMemory();
            Assert.AreEqual(mem.Tags.Count, 0);
        }
        [TestMethod]
        public void TestInvalidLastTag()
        {
            Modelo.DeviceMemory mem = new Modelo.DeviceMemory();
            Assert.IsFalse(mem.isLastTagValid);
        }
        [TestMethod]
        public void TestTagsEmptyReadQtd()
        {
            Modelo.DeviceMemory mem = new Modelo.DeviceMemory();
            Assert.AreEqual(mem.TagsReadQty, 0);
        }
        [TestMethod]
        public void TestTagsAddNullTag()
        {
            Modelo.DeviceMemory mem = new Modelo.DeviceMemory();
            mem.AddTag(null);
            Assert.AreEqual(mem.TagsReadQty, 0);
        }
        [TestMethod]
        public void TestTagsAddEmptyTag()
        {
            Modelo.DeviceMemory mem = new Modelo.DeviceMemory();
            mem.AddTag(" ");
            Assert.AreEqual(mem.TagsReadQty, 0);
        }
        [TestMethod]
        public void TestTagsAdd1Tag()
        {
            Modelo.DeviceMemory mem = new Modelo.DeviceMemory();
            mem.AddTag("123");
            Assert.AreEqual(mem.TagsReadQty, 1);
        }
        [TestMethod]
        public void TestTagsAdd3IguaisTag()
        {
            Modelo.DeviceMemory mem = new Modelo.DeviceMemory();
            mem.AddTag("123");
            mem.AddTag("123");
            mem.AddTag("123");
            Assert.AreEqual(1, mem.TagsReadQty);
        }
        [TestMethod]
        public void TestTagsAdd3IguaisTagQtd()
        {
            Modelo.DeviceMemory mem = new Modelo.DeviceMemory();
            mem.AddTag("123");
            mem.AddTag("123");
            mem.AddTag("123");
            Assert.AreEqual(3, mem.LastTag.Qtd);
        }
        [TestMethod]
        public void TestTagsAdd3DiferentesTags()
        {
            Modelo.DeviceMemory mem = new Modelo.DeviceMemory();
            mem.AddTag("123");
            mem.AddTag("456");
            mem.AddTag("789");
            Assert.AreEqual(3, mem.TagsReadQty);
        }
    }
}
