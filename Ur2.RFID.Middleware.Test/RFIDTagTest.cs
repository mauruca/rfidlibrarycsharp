﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ur2.RFID.Middleware;

namespace Ur2.RFID.Middleware.Test
{
    [TestClass]
    public class RFIDTagTest
    {
        [TestMethod]
        public void TestTagInvalido()
        {
            Modelo.RFIDTag tag = new Modelo.RFIDTag();
            Assert.IsFalse(tag.isValid);
        }
        [TestMethod]
        public void TestTagIdVazio()
        {
            Modelo.RFIDTag tag = new Modelo.RFIDTag();
            Assert.AreEqual(tag.Id, string.Empty);
        }
        [TestMethod]
        public void TestTagIdInsereNull()
        {
            Modelo.RFIDTag tag = new Modelo.RFIDTag();
            tag.Id = null;
            Assert.AreEqual(tag.Id, string.Empty);
        }
        [TestMethod]
        public void TestTagIdConstrutor()
        {
            Random rnd = new Random();
            int n = rnd.Next(0, int.MaxValue);
            Modelo.RFIDTag tag = new Modelo.RFIDTag(n.ToString());
            Assert.AreEqual(tag.Id, n.ToString());
        }
        [TestMethod]
        public void TestTagIdPropriedade()
        {
            Random rnd = new Random();
            int n = rnd.Next(0, int.MaxValue);
            Modelo.RFIDTag tag = new Modelo.RFIDTag();
            tag.Id = n.ToString();
            Assert.AreEqual(tag.Id, n.ToString());
        }
        [TestMethod]
        public void TestTagQtdZerada()
        {
            Modelo.RFIDTag tag = new Modelo.RFIDTag();
            Assert.AreEqual(tag.Qtd, 0);
        }
        [TestMethod]
        public void TestTagQtdMaisRegistro()
        {
            Modelo.RFIDTag tag = new Modelo.RFIDTag();
            tag.MaisUmRegistro();
            Assert.AreEqual(tag.Qtd, 1);
        }
        [TestMethod]
        public void TestTagQtdMaisNRegistro()
        {
            Modelo.RFIDTag tag = new Modelo.RFIDTag();
            Random rnd = new Random();
            int n = rnd.Next(0, 50);
            int i;
            for (i = 0; i< n;i++)
                tag.MaisUmRegistro();
            Assert.AreEqual(tag.Qtd, i);
        }
        [TestMethod]
        public void TestTagToString()
        {
            Random rnd = new Random();
            int n = rnd.Next(0, int.MaxValue);
            Modelo.RFIDTag tag = new Modelo.RFIDTag(n.ToString());
            Assert.AreEqual(tag.ToString(), n.ToString());
        }
        [TestMethod]
        public void TestTagFalseEquals()
        {
            Random rnd = new Random();
            int n = rnd.Next(10, 20);
            Modelo.RFIDTag tag = new Modelo.RFIDTag(n.ToString());
            Assert.IsFalse(tag.Equals(new Modelo.RFIDTag("1")));
        }
        [TestMethod]
        public void TestTagEquals()
        {
            Random rnd = new Random();
            int n = rnd.Next(0, int.MaxValue);
            Modelo.RFIDTag tag = new Modelo.RFIDTag(n.ToString());
            Assert.IsTrue(tag.Equals(new Modelo.RFIDTag(n.ToString())));
        }
    }
}
